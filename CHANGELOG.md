# [Scene References] :: [Changelog]

## [0.3.0] - 2021-09-08
### Added
- Tooltip over object field shows project-relative path to asset.

## [0.2.1] - 2021-09-08
### Fixed
- Fix #2: Property labels in array items appearing in other tool windows.

## [0.2.0] - 2021-06-24
### Fixed
- Using a trick to draw missing references properly in the inspector.

## [0.1.1] - 2021-06-24
### Fixed
- Using older C# code for compatibility with Unity 2019.
- Property label fixed to no longer show the GUID when inside an array.

## [0.1.0] - 2021-06-16
### Added
- First release of Scene References as a package.

[Scene References]: https://bitbucket.org/alfish/com.unity_x.modules.sceneref/
[Changelog]: https://bitbucket.org/alfish/com.unity_x.modules.sceneref/src/HEAD/CHANGELOG.md
[0.3.0]: https://bitbucket.org/alfish/com.unity_x.modules.sceneref/branches/compare/v0.3.0..v0.2.1
[0.2.1]: https://bitbucket.org/alfish/com.unity_x.modules.sceneref/branches/compare/v0.2.1..v0.2.0
[0.2.0]: https://bitbucket.org/alfish/com.unity_x.modules.sceneref/branches/compare/v0.2.0..v0.1.1
[0.1.1]: https://bitbucket.org/alfish/com.unity_x.modules.sceneref/branches/compare/v0.1.1..v0.1.0
[0.1.0]: https://bitbucket.org/alfish/com.unity_x.modules.sceneref/branches/compare/v0.1.0..db4a29
