# Scene References

This package allows referencing a scene asset at runtime. Since the reference is stored reliably by GUID, you can move and rename scene assets and change build indices freely.

## Usage
Simply use the `SceneReference` struct in a field, then use the inspector to assign a scene object to the property. Builds will include an asset to map each scene's serialized GUID to its build-index.

Example usage in a typical script:
```cs
using UnityEngine;
// Property is drawn like SceneAsset in the inspector. You can drag-drop, etc.
public SceneReference sceneToLoad;
// Use the sceneIndex property at runtime to load the scene:
var loading = SceneManager.LoadSceneAsync(sceneToLoad.sceneIndex);
```

## Legal
This package is in public domain, so feel free to use it without attribution.

## Additional Info
Check the [wiki] for installation instructions and support. Contributions are welcome!

[wiki]: https://bitbucket.org/alfish/com.unity_x.modules.sceneref/wiki/Home
